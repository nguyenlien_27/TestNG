package common;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import pageObject.logInObject;

public class commonfuns {
	public static String ExcelName = "testData.xlsx";
    public static String SheetName = "logIn";
	public static void commonLogIn() throws Exception {
		utils.driver.get(utils.URL);
		XSSFSheet ExcelDataSheet = ExcelCommon_POI.setExcelFile(ExcelName, SheetName);
		String User = ExcelCommon_POI.getCellData(1, 1, ExcelDataSheet);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataSheet);
		
		utils.driver.findElement(logInObject.txtUsername).sendKeys(User);
		utils.driver.findElement(logInObject.txtPassword).sendKeys(Password);
		utils.driver.findElement(logInObject.btnSubmit).click();
		Thread.sleep(8000);
	}

	public static void commonLogOut() {
		utils.driver.get("http://localhost/pentest1/wp-login.php?action=logout");
		utils.driver.findElement(By.xpath(".//*[@id='error-page']/p[2]/a")).click();
	}
}
