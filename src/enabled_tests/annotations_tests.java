package enabled_tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class annotations_tests {
	@Test (priority = 2)
	 
    public void testCase1() {
         System.out.println("Test Case 1");
    }
 
    @Test (priority = 1)
     public void testCase2() {
         System.out.println("Test Case 2");
     }
 
    @BeforeMethod
     public void beforeMethod() {
         System.out.println("Before (Every) Method");
     }
 
    @AfterMethod
     public void afterMethod() {
         System.out.println("After (Every) Method");
     }
 
    @BeforeClass
     public void beforeClass() {
         System.out.println("Before the Class");
     }
 
    @AfterClass
     public void afterClass() {
         System.out.println("After the Class");
     }
 
    @BeforeTest
     public void beforeTest() {
         System.out.println("Before the Test!!!");
     }
 
    @AfterTest
     public void afterTest() {
         System.out.println("After the Test!!!");
     }
 
    @BeforeSuite
     public void beforeSuite() {
         System.out.println("Before Test Suite...");
     }
 
    @AfterSuite
     public void afterSuite() {
         System.out.println("After Test Suite...");
     }
}

