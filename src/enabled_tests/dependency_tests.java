package enabled_tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class dependency_tests {
	private WebDriver driver;
	private String URL = "http://harveynash.com";

	@BeforeTest
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test(dependsOnMethods = "verifyURL")
	public void verifyHomepageTitle() {
		String expectedTitle = "Harvey Nash � Global Recruitment Experts";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(actualTitle, expectedTitle);
		System.out.println("The title is " + actualTitle);
	}

	@Test(dependsOnMethods = "loadURL")
	public void verifyURL() {
		String expectedURL = "http://harveynash.com/";
		String actualURL = driver.getCurrentUrl();
		Assert.assertEquals(actualURL, expectedURL);
		System.out.println("The URL is " + actualURL);
	}

	@Test
	public void loadURL() {
		driver.get(URL);
	}

	@AfterTest
	public void closeBrowser() {
		driver.quit();
	}
}
