package enabled_tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class enabled_tests {
	private WebDriver driver;

	@BeforeTest
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://harveynash.com");
	}
	//khai bao = true -> chay ham nay
	//priority = khai bao thu tu chay cac @test. bat dau tu cai nho nhat (ex: 1, 2...)
	@Test (enabled = true, priority = 2)
	public void verifyHomepageTitle() {
		String expectedTitle = "Harvey Nash � Global Recruitment Experts";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(expectedTitle, actualTitle);
		System.out.println("The title is " + actualTitle);
	}
	//khai bao = false -> bo qua ko chay
	@Test (enabled = true, priority = 2)
	public void verifyURL() {
		String expectedURL = "http://harveynash.com/";
		String actualURL = driver.getCurrentUrl();
		Assert.assertEquals(expectedURL, actualURL);
		System.out.println("The URL is " + actualURL);
	}

	@AfterTest
	public void closeBrowser() {
		driver.quit();
	}

}
