package test;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.*;
import pageObject.addNewUserObject;
import pageObject.allUserObject;

public class addUser {
	@BeforeTest
	public void before() {
		System.getProperty("webdriver.gecko.driver", "geckodriver.exe");
		utils.driver = new FirefoxDriver();
	}

	@Test (priority = 1)
	public void logIn() throws Exception{
		commonfuns.commonLogIn();
	}
	
	@Test (priority = 2)
	public void checkUser() throws InterruptedException{
		// load add all user page
		utils.driver.findElement(allUserObject.mnuAlluser).click();
		Thread.sleep(3000);
		// check user is exist or not
		//input search text box
		utils.driver.findElement(allUserObject.txtSearchuser).sendKeys("user01");
		//click on search button to do search
		utils.driver.findElement(allUserObject.btnSearchuser).click();
		Thread.sleep(3000);
		
	}
	@Test (priority = 3)
	public void addUserAndCheck() throws InterruptedException{
		//check user is exist or not
		if(utils.driver.getPageSource().contains("No matching users were found.")){
			System.out.println("No matching -> create new user");
		} else {
			//delete the user
			//check all user list
			utils.driver.findElement(allUserObject.chkAlluser).click();
			//select delete action
			utils.driver.findElement(allUserObject.slbAction).sendKeys("Delete");
			Thread.sleep(3000);
			//click on apply button to to search
			utils.driver.findElement(allUserObject.btnDoaction).click();
			Thread.sleep(5000);
			//choose delete
			utils.driver.findElement(allUserObject.rdoDeleteoption).click();
			//click on delete button to do
			utils.driver.findElement(allUserObject.btnDeleteuser).click();
			Thread.sleep(5000);
		}
		//go to add new user screen then input info for new user
		//click on add new user menu
		utils.driver.findElement(addNewUserObject.mnuAddnewuser).click();
		Thread.sleep(3000);
		//input user info
		utils.driver.findElement(addNewUserObject.txtUsername).sendKeys("user01");
		utils.driver.findElement(addNewUserObject.txtEmail).sendKeys("nguyenlien.7@gmail.com");
		utils.driver.findElement(addNewUserObject.txtPass1).sendKeys("123456");
		utils.driver.findElement(addNewUserObject.txtPass2).sendKeys("123456");
		utils.driver.findElement(By.id("createusersub")).click();
		Thread.sleep(5000);
	
	}
	
	@Test (priority = 4)
	public void logOut(){
		commonfuns.commonLogOut();
	}
	@AfterTest
	public void after(){
		utils.driver.quit();	
	}
}
