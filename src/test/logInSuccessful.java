package test;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.utils;
import pageObject.dashboardObject;
import pageObject.logInObject;

public class logInSuccessful {
	@BeforeTest
	public void before() {
		System.setProperty("webdriver.gecko.dirver", utils.Geckodriver);
		utils.driver = new FirefoxDriver();
	}

	@Test
	public void logIn() throws InterruptedException {
	//login
		utils.driver.get(utils.URL);
		utils.driver.findElement(logInObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(logInObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(logInObject.btnSubmit).click();
		Thread.sleep(8000);
		String VerifyTextLoginSucessfull = utils.driver.findElement(dashboardObject.verifyLoginSuccessfull).getText();
		Assert.assertEquals("Dashboard", VerifyTextLoginSucessfull);
	}
	
	@Test 
	public void takeScreenShot() throws IOException {
		//get current date time
		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
		//get current date time with Date()
		Date date = new Date();
		 
		 // Now format the date
		 String currentdatetime = dateFormat.format(date);
		 System.out.println(currentdatetime);
		
		//take screen short
		File src= ((TakesScreenshot)utils.driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/"+"dashboard_"+currentdatetime+".png"));
	}

	@AfterTest
	public void after() throws Exception {
		utils.driver.quit();
	}
}
