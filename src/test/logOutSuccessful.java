package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.utils;
import pageObject.dashboardObject;
import pageObject.logInObject;

public class logOutSuccessful {
	@BeforeTest
	public void before(){
		System.setProperty("webdriver.gecko.drive",utils.Geckodriver);
		utils.driver = new FirefoxDriver();
	}
	
	@Test
	public void logIn() throws InterruptedException {
	//login
		utils.driver.get(utils.URL);
		utils.driver.findElement(logInObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(logInObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(logInObject.btnSubmit).click();
		Thread.sleep(8000);
		String VerifyTextLoginSucessfull = utils.driver.findElement(dashboardObject.verifyLoginSuccessfull).getText();
		Assert.assertEquals("Dashboard", VerifyTextLoginSucessfull);
		Thread.sleep(4000);
	}
	

	@Test
	public void logOut() throws InterruptedException{
		WebElement element = utils.driver.findElement(By.xpath(".//*[@id='wp-admin-bar-my-account']/a"));
	    Actions action = new Actions(utils.driver);
	    action.moveToElement(element).perform();
	    Thread.sleep(4000);
	    WebElement subElement = utils.driver.findElement(By.linkText("Log Out"));
	    action.moveToElement(subElement);
	    action.click();
	    action.perform();
	}
	
}
